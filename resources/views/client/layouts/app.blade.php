<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="/assets/images/layout/favicon.png">
    <title>
        @yield('title', 'MyBrand')
    </title>

    {{-- FONTS --}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    {{-- STYLES --}}
    <link rel="stylesheet" href="/assets/static/css/plumcss/plumcss.css">
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    @yield('styles')

</head>
<body>
    <div id="app" class="root-container"></div>

    {{-- SCRIPTS --}}
    <script src="{{ mix('/js/app.js') }}"></script>
    @yield('scripts')
</body>
</html>
