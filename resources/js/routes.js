const home = () => import('./components/views/home/Home.vue');
const successStory = () => import('./components/views/success_story/SuccessStory.vue');

export const routes = [
    {
        name: 'home',
        path: '/',
        component: home
    },
    {
        name: 'caso-de-exito',
        path: '/caso-de-exito/:id',
        component: successStory
    }
]