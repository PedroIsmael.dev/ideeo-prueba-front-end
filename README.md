Instalacion de proyecto

Esta es la página

https://picsum.photos/

end point: https://picsum.photos/v2/list

Se requiere composer, node, npm.

Despues de clonar el proyecto: $ git clone git@gitlab.com:PedroIsmael.dev/ideeo-prueba-front-end.git

Agregar el archivo de variables .env en la carpeta raiz del proyecto y agregar la siguiente configuracion:

APP_NAME=Mybrand
APP_ENV=local
APP_KEY=base64:rqM7v+cDhDQ6N/tEInQQ2ndZRw9f1v3khBhMQdYLCdE=
APP_DEBUG=true
APP_URL=http://localhost:8000

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DRIVER=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

una ves agregado ejecutar los siguientes comandos:

    $ npm install
    $ npm run dev
    $ composer install
    $ php artisan config:cache
    $ php artisan route:cache
    $ php artisan serve

una vez levantado el servidor ir a la ruta local: localhost:8000/

